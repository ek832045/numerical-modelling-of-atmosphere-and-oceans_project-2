import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import uniform_filter
import function as f
from __init__ import consts
from matplotlib import cm

dx = 10**4
dy = 10**4
eta_0 = -0.11258543674198031 # Found from task below
X, Y, u_st, v_st, eta_st = f.steady_state_sol(dx, dy, eta_0)


fig1 = plt.figure(1)
plt1 = plt.contourf(X, Y, u_st, cmap=cm.coolwarm)
cbar1 = fig1.colorbar(plt1, label='u (ms$^{-1}$)')
plt.title('(a) u -Analytical solution')
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")

fig2 = plt.figure(2)
plt2 = plt.contourf(X, Y, v_st, cmap=cm.coolwarm)
cbar2 = fig2.colorbar(plt2, label='v (ms$^{-1}$)')
plt.title('(b) v -Analytical solution')
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")

fig3 = plt.figure(3)
plt3 = plt.contourf(X, Y, eta_st, cmap=cm.coolwarm)
cbar3 = fig3.colorbar(plt3, label='Depth anomaly (m)')
plt.title('(c) $\eta$ -Analytical solution')
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")

fig4 = plt.figure(4)
color = np.hypot(u_st, v_st)
plt4 = plt.streamplot(X, Y, u_st, v_st, color=color, cmap=cm.coolwarm)
cbar4 = fig4.colorbar(plt4.lines,label='ms$^{-1}$')
plt.title('(d) Steady State Speed -Analytical solution')
plt.xlabel("$x$ (m)")
plt.ylabel("$y$ (m)")