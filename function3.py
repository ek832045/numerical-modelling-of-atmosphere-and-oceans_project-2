import numpy as np
import matplotlib.pyplot as plt
import sys 
from __init__ import consts
from scipy.ndimage import uniform_filter
from matplotlib import cm

def run_model(dx, dy, dt, nt, consts=consts, output_energy=False):
  
  L = consts['L']
  nx = int(L/dx)
  ny = int(L/dy)

  X_u, Y_u, X_v, Y_v = Arakawa_C_grid(dx, dy)
  eta, u, v = init_arakawa_C(nx, ny)
  tau_x_u = Caculate_tau_x(Y_u)
  tau_y_v = np.zeros_like(X_v)
  energy = []

  # iteration order
  u_before_v_flag = True
  

  for t in range(nt):
    eta_new = iterate_eta(eta, u, v, dx, dy, dt)

    if u_before_v_flag:
      # iterate u first
      u_new = iterate_u(u, v, eta_new, Y_u, tau_x_u, dx, dt)
      v_new = iterate_v(u_new, v, eta_new, Y_v, tau_y_v, dy, dt)
    else:
      # Iterate v first
      v_new = iterate_v(u, v, eta_new, Y_v, tau_y_v, dy, dt)
      u_new = iterate_u(u, v_new, eta_new, Y_u, tau_x_u, dx, dt)
        
    energy.append(Perturbation_energy(u_new, v_new, eta_new, dx, dy))

    eta = eta_new.copy()
    u = u_new.copy()
    v = v_new.copy()
    u_before_v_flag = not u_before_v_flag
   
  return [u, v, eta, energy]

# create arakawa c grids 
def Arakawa_C_grid(dx, dy, consts=consts, Caculate_or_plot='Calculate'):
    # Create arrays of size l
    x = np.arange(0, consts['L'], dx)
    y = np.arange(0, consts['L'], dy)
    # Add dx,dy for the l+1 arrays which need an extra gridpoint
    x_plus_one = np.arange(0, consts['L']+dx, dx)
    y_plus_one = np.arange(0, consts['L']+dy, dy)

    # Shift gridpoints by a factor 0.5 * dx_dy for interpolated grids 
    x_v = x + 0.5*dx
    y_u = y + 0.5*dy

    # Create meshgrids for the u and v
    # u grid needs one extra point in x, interpolated values in y
    # v grid needs one extra point in y, interpolated values in x
    X_u, Y_u = np.meshgrid(x_plus_one, y_u)
    X_v, Y_v = np.meshgrid(x_v, y_plus_one)

    if Caculate_or_plot == 'Calculate':
        return [X_u, Y_u, X_v, Y_v]
    elif Caculate_or_plot == 'plot':
        return [x, y, x_plus_one, y_plus_one]
    else:
        return None
    
def init_arakawa_C(nx, ny):
    # For the Arakawa C grid, the dimensions for the data are:
    # eta: [ny  , nx  ]
    # u  : [ny  , nx+1]
    # v  : [ny+1, nx  ]

    eta_array = np.zeros((ny, nx))
    u_array = np.zeros((ny, nx+1))
    v_array = np.zeros((ny+1, nx))

    return[eta_array, u_array, v_array]

# Functions to precalculate tau matrices
def Caculate_tau_x(Y, consts=consts):
    tau_0 = consts['tau_0']
    L = consts['L']
    tau_x = -tau_0 * np.cos(np.pi*Y/L)
    # Now apply BCs
    tau_x[:,0] = 0
    tau_x[:,-1] = 0
    return tau_x

# Iterating functions
def iterate_eta(eta, u, v, dx, dy, dt, consts=consts):
    H = consts['H']
    dudx = np.diff(u, axis=1)/dx
    dvdy = np.diff(v, axis=0)/dy
    eta_new = eta - H*dt*(dudx + dvdy)
    return eta_new

def iterate_u(u, v, eta, Y, tau_x, dx, dt, consts=consts):
    rho = consts['rho']
    H = consts['H']
    g = consts['g']
    gamma = consts['gamma']
    f0 = consts['f0']
    beta = consts['beta']

    # Use filter function to get the average of four neighbouring v values on u grid
    v_filtered = uniform_filter(v, size=2, mode='constant', cval=0, origin=(-1,0))
    v_filtered = v_filtered[:-1,:]
    v_filtered[:,0] = 0
    v_filtered = np.pad(v_filtered, pad_width=((0,0),(0,1)))

    d_eta_dx = np.diff(eta, axis=1)/dx
    d_eta_dx = np.pad(d_eta_dx, pad_width=((0,0),(1,1)))

    corli = (f0 + beta*Y) * dt * v_filtered   # coriolis term
    eta_grid = -d_eta_dx * g * dt                      # eta grad term
    tau_x_term = tau_x*dt/(rho*H)                   # tau term 
    gamma_term = u * (1 - gamma*dt)                 # u and gamma terms
    return corli+eta_grid+tau_x_term+gamma_term

def iterate_v(u, v, eta, Y, tau_y, dy, dt, consts=consts):
    rho = consts['rho']
    H = consts['H']
    g = consts['g']
    gamma = consts['gamma']
    f0 = consts['f0']
    beta = consts['beta']

    # Use filter function to get the average of four neighbouring u values on v grid
    u_filtered = uniform_filter(u, size=2, mode='constant', cval=0, origin=(0,-1))
    u_filtered = u_filtered[:,:-1]
    u_filtered[0,:] = 0
    u_filtered = np.pad(u_filtered, pad_width=((0,1),(0,0)))

    d_eta_dy = np.diff(eta, axis=0)/dy
    d_eta_dy = np.pad(d_eta_dy, pad_width=((1,1),(0,0)))

    corli = -(f0 + beta*Y) * dt * u_filtered   # coriolis term
    eta_grid = -d_eta_dy * g * dt                       # eta grad term
    tau_y_term = tau_y*dt/(rho*H)                    # tau term
    gamma_term = v * (1 - gamma*dt)                  # v and gamma terms
    return corli+eta_grid+tau_y_term+gamma_term

# Other Functions
def reshape_arrays_to_equal_size(u_array, v_array):

    pad_array = np.zeros(len(u_array[0]))
    u_array = np.vstack([u_array, pad_array])
    v_array = np.pad(v_array, (0,1))[:-1,:]
    # eta_array = np.pad(eta_array, (0,1))
    return [u_array, v_array]

def Perturbation_energy(u, v, eta, dx, dy, consts=consts):
    rho = consts['rho']
    H = consts['H']
    g = consts['g']
    # First, reshape u and v arrays to be of equal size:
    u, v = reshape_arrays_to_equal_size(u, v)

    # Now, reshape eta array to be of same size as above
    # TODO: make a function for this?
    eta = np.pad(eta, (0,1))

    wind_speed = u**2 + v**2
    energy_per_gridpoint = 0.5*rho*dx*dy * (H*wind_speed + g*eta**2)
    total_energy = np.sum(energy_per_gridpoint)
    return total_energy

def plot_energy(plt_id, energy, dt, nt, dx):

    # Create time array, in days
    t = np.arange(0, dt*nt, dt)
    t = t/(60*60*24)
    
    fig = plt.figure(plt_id)
    plt11 = plt.plot(t, energy, label='Perturbation energy')
    plt.xlabel("Time (days)")
    plt.ylabel("Perturbation Energy (J)")
    plt.title('Energy with time') 
        
def plot_Task_D1(dy, x, y, x_plus_one, y_plus_one, u, v, eta, \
                plt_id = 0, y_lim = [-0.1,0.1], legend_loc= 'upper right'): 
    
    u, v = reshape_arrays_to_equal_size(u, v)
    
    L = consts['L']
    X_uv, Y_uv = np.meshgrid(x_plus_one, y_plus_one)
    
    fig2 = plt.figure(plt_id)
    plt2 = plt.contourf(X_uv, Y_uv, u, cmap=cm.coolwarm)
    cbar1 = fig2.colorbar(plt2, label='u (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('u -Numerical solution (1 Day)')
    
    fig3 = plt.figure(plt_id+1)
    plt3 = plt.contourf(X_uv, Y_uv, v, cmap=cm.coolwarm)
    cbar2 = fig3.colorbar(plt3, label='v (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('v -Numerical solution (1 Day)')
    
    X_eta, Y_eta = np.meshgrid(x, y)
    fig4 = plt.figure(plt_id+2)
    plt4 = plt.contourf(X_eta, Y_eta, eta, cmap=cm.coolwarm)
    cbar4 = fig4.colorbar(plt4, label='Height (m)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('$\eta$ -Numerical solution (1 Day)')
    
    fig5 = plt.figure(plt_id+3)
    plt4 = plt.plot(x_plus_one, u[0,:],'b',label='u along south boundary')
    plt4 = plt.plot(x_plus_one, v[:,0],'r',label='v along west boundary')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel("wind (ms$^{-1}$)")
    plt.title('Numerical solution (1 Day)')
    plt.legend(loc=legend_loc,fontsize= 12)
    
    fig5 = plt.figure(plt_id+4)
    ny_half = int(consts['L']/(2*dy))
    plt5 = plt.plot(x, eta[ny_half,:],'g',label='$\eta$ through the middle of gyre')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel('Height (m)')
    plt.title('Numerical solution (1 Day)')
    plt.legend(loc=legend_loc,fontsize= 12)
    
def plot_Task_D30(dy, x, y, x_plus_one, y_plus_one, u, v, eta, \
                plt_id = 0, y_lim = [-0.1,0.1], legend_loc= 'upper right'): 
    
    u, v = reshape_arrays_to_equal_size(u, v)
    
    L = consts['L']
    X_uv, Y_uv = np.meshgrid(x_plus_one, y_plus_one)
    
    fig2 = plt.figure(plt_id)
    plt2 = plt.contourf(X_uv, Y_uv, u, cmap=cm.coolwarm)
    cbar1 = fig2.colorbar(plt2, label='u (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('u -Numerical solution (30 Days)')
    
    fig3 = plt.figure(plt_id+1)
    plt3 = plt.contourf(X_uv, Y_uv, v, cmap=cm.coolwarm)
    cbar2 = fig3.colorbar(plt3, label='v (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('v -Numerical solution (30 Days)')
    
    X_eta, Y_eta = np.meshgrid(x, y)
    fig4 = plt.figure(plt_id+2)
    plt4 = plt.contourf(X_eta, Y_eta, eta, cmap=cm.coolwarm)
    cbar4 = fig4.colorbar(plt4, label='Height (m)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('$\eta$ -Numerical solution (30 Days)')
    
    fig5 = plt.figure(plt_id+3)
    plt4 = plt.plot(x_plus_one, u[0,:],'b',label='u along south boundary')
    plt4 = plt.plot(x_plus_one, v[:,0],'r',label='v along west boundary')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel("wind (ms$^{-1}$)")
    plt.title('Numerical solution (30 Days)')
    plt.legend(loc=legend_loc,fontsize= 12)
    
    fig5 = plt.figure(plt_id+4)
    ny_half = int(consts['L']/(2*dy))
    plt5 = plt.plot(x, eta[ny_half,:],'g',label='$\eta$ through the middle of gyre')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel('Height (m)')
    plt.title('Numerical solution (30 Days)')
    plt.legend(loc=legend_loc,fontsize= 12)
    
    
def plot_Task_Ddiff(dy, x, y, x_plus_one, y_plus_one, u, v, eta, \
                plt_id = 0, y_lim = [-0.1,0.1], legend_loc= 'upper right'): 
    
    u, v = reshape_arrays_to_equal_size(u, v)
    
    L = consts['L']
    X_uv, Y_uv = np.meshgrid(x_plus_one, y_plus_one)
    
    fig2 = plt.figure(plt_id)
    plt2 = plt.contourf(X_uv, Y_uv, u, cmap=cm.coolwarm)
    cbar1 = fig2.colorbar(plt2, label='u (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('The difference of u')
    
    fig3 = plt.figure(plt_id+1)
    plt3 = plt.contourf(X_uv, Y_uv, v, cmap=cm.coolwarm)
    cbar2 = fig3.colorbar(plt3, label='v (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('The difference of v')
    
    X_eta, Y_eta = np.meshgrid(x, y)
    fig4 = plt.figure(plt_id+2)
    plt4 = plt.contourf(X_eta, Y_eta, eta, cmap=cm.coolwarm)
    cbar4 = fig4.colorbar(plt4, label='Height (m)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('The difference of $\eta$')
       
