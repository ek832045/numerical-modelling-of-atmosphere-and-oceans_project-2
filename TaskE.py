import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import uniform_filter
import function as f
import function3 as f3
from __init__ import consts

dx = 10**4   #m
dy = 10**4   #m
dt = 70      #s
nt = int(50*86400/dt)   

x, y, x_plus_one, y_plus_one = f3.Arakawa_C_grid(dx, dy, consts, 'plot')
u, v, eta, energy = f3.run_model(dx, dy, dt, nt, output_energy=True)

f3.plot_energy(20, energy, dt, nt, dx)
    
# halve the grid-spacing of your model
dx = 5000  #m
dy = 5000   #m
dt = 30    #s
nt = int(50*86400/dt)  

x, y, x_plus_one, y_plus_one = f3.Arakawa_C_grid(dx, dy, consts, 'plot')
u, v, eta, energy = f3.run_model(dx, dy, dt, nt, output_energy=True)

f3.plot_energy(25, energy, dt, nt, dx)

eta_0 = -0.11258543674198031 # Found from task below
_, _, u_st, v_st, eta_st = f.steady_state_sol(dx, dy, eta_0)
u_st = np.pad(u_st, pad_width=((0,0),(0,1)))
v_st = np.pad(v_st, pad_width=((0,1),(0,0)))
u_diff = u - u_st
v_diff = v - v_st
eta_diff = eta - eta_st

energy_diff = f3.Perturbation_energy(u_diff, v_diff, eta_diff, dx, dy)
print('enery from difference field = ', energy_diff)

eta_0 = eta[100,0]
print(eta_0)