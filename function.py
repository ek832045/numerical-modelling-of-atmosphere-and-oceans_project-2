import numpy as np
import matplotlib.pyplot as plt
import sys 
from __init__ import consts

def steady_state_sol(dx, dy, eta_0, consts=consts):
    tau_0 = consts['tau_0']
    rho = consts['rho']
    H = consts['H']
    L = consts['L']
    gamma = consts['gamma']
    g = consts['g']
    f0 = consts['f0']
    beta = consts['beta']
    
    coeff = tau_0/(np.pi*gamma*rho*H)
    
    x = np.arange(0, consts['L'], dx)
    y = np.arange(0, consts['L'], dx) 
    nx = int(consts['L']/dx)
    ny = int(consts['L']/dy)
    
    X, Y = np.meshgrid(x, y) 

    u_st = -coeff*np.multiply(f1(X/L),np.cos(np.pi*Y/L))
    v_st = coeff*np.multiply(f2(X/L),np.sin(np.pi*Y/L))
    eta_st = coeff*f0*L/g*eta_term(X, Y)

    return [X, Y, u_st, v_st, eta_st]

def f1(X, consts=consts):
    a = consts['a']
    b = consts['b']

    c = ((np.exp(a)-1)*np.exp(b*X) + (1-np.exp(b))*np.exp(a*X))/(np.exp(b) - np.exp(a))
    f = np.pi*(1 + c)
    return f

def f2(X, consts=consts):
    a = consts['a']
    b = consts['b']

    f = ((np.exp(a)-1)*b*np.exp(b*X) + (1 - np.exp(b))*a*np.exp(a*X))/(np.exp(b) - np.exp(a))
    return f

def eta_term(X, Y, consts=consts):
    tau_0 = consts['tau_0']
    rho = consts['rho']
    H = consts['H']
    L = consts['L']
    gamma = consts['gamma']
    g = consts['g']
    f0 = consts['f0']
    beta = consts['beta']

    f2_xy = gamma/(f0*np.pi)*np.multiply(f2(X/L),np.cos(np.pi*Y/L))
    f1_y = np.multiply(np.sin(np.pi*Y/L),(1+beta*Y/f0))+beta*L/(f0*np.pi)*np.cos(np.pi*Y/L)
    f1_xy = 1./np.pi*np.multiply(f1(X/L),f1_y)
    f= f2_xy+f1_xy
    
    return f


