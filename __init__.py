import numpy as np
import matplotlib.pyplot as plt

consts = {
    'f0'   : 10**-4,
    'beta' : 10**-11,
    'g'    : 10,
    'gamma': 10**-6,
    'rho'  : 1000,
    'H'    : 1000,
    'L'    : 10**6,
    'tau_0': 0.2,
}

epsilon = consts['gamma']/(consts['L']*consts['beta'])
a = (-1 - np.sqrt(1 + (2*np.pi*epsilon)**2)) / (2*epsilon)
b = (-1 + np.sqrt(1 + (2*np.pi*epsilon)**2)) / (2*epsilon)

consts['epsilon'] = epsilon
consts['a'] = a
consts['b'] = b