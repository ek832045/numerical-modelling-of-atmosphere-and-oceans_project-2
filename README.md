For the results in the form of a Python notebook:
Run MTMW14_Project2.ipynb

For the results and plots of QC:
Run __init__.py
Run function.py
Run TaskC.py

For the results and plots of QD:
Run __init__.py
Run function.py
Run function3.py
Run TaskD.py

For the results and plots of QE:
Run __init__.py
Run function.py
Run function3.py
Run TaskE.py

For the results and plots of QF:
Run __init__.py
Run function.py
Run function3.py
Run Semi_Lag.py
Run TaskF.py
