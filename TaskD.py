import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import uniform_filter
import function as f
import function3 as f3
from __init__ import consts

# STEP 1: 
dx = 10**4   #m
dy = 10**4   #m
dt = 70      #s
nt = int(86400/dt)

u, v, eta, energy = f3.run_model(dx, dy, dt, nt, output_energy=True)
x, y, x_plus_one, y_plus_one = f3.Arakawa_C_grid(dx, dy, consts, 'plot')
f3.plot_Task_D1(dy, x, y, x_plus_one, y_plus_one, u, v, eta, \
               plt_id = 0, y_lim = [-0.02,0.03], legend_loc = 'upper right')
    
# STEP 2: 
dx = 10**4   #m
dy = 10**4   #m
dt = 70      #s
nt = int(30*86400/dt)   

u, v, eta, energy = f3.run_model(dx, dy, dt, nt, output_energy=True)
x, y, x_plus_one, y_plus_one = f3.Arakawa_C_grid(dx, dy, consts, 'plot')
f3.plot_Task_D30(dy, x, y, x_plus_one, y_plus_one, u, v, eta, \
               plt_id = 5, y_lim = [-0.2,0.5], legend_loc = 'upper right')
    
# STEP 3:
dx = 10**4   #m
dy = 10**4   #m
dt = 70      #s
nt = int(30*86400/dt)   

u, v, eta, energy = f3.run_model(dx, dy, dt, nt, output_energy=True)
x, y, x_plus_one, y_plus_one = f3.Arakawa_C_grid(dx, dy, consts, 'plot')    
eta_0 = -0.11258543674198031 # Found from task below
_, _, u_st, v_st, eta_st = f.steady_state_sol(dx, dy, eta_0)
u_st = np.pad(u_st, pad_width=((0,0),(0,1)))
v_st = np.pad(v_st, pad_width=((0,1),(0,0)))
u_diff = u - u_st
v_diff = v - v_st
eta_diff = eta - eta_st

f3.plot_Task_Ddiff(dy, x, y, x_plus_one, y_plus_one, u_diff, v_diff, eta_diff, \
               plt_id = 10, legend_loc = 'upper right')

