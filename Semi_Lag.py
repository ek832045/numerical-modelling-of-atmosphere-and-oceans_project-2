import numpy as np
import matplotlib.pyplot as plt
import function as f
import function3 as f3
from scipy import interpolate
from __init__ import consts
from matplotlib import cm

# parameter setup
L, f_0, beta, g, gamma, rho, H, tau_0 = consts['L'], consts['f0'], consts['beta'], \
       consts['g'], consts['gamma'], consts['rho'], consts['H'], consts['tau_0']     
epsilon = gamma/(L*beta)                   
  
def semi_lagrangian(dx, dy, dt, nt):
    nx = int(L/dx)
    ny = int(L/dy)
    """Loop Semi-Lagrangian scheme over time"""
    
    x, y, x_u, y_v = f3.Arakawa_C_grid(dx, dy, consts, 'plot')
    x_v = x + 0.5*dx
    y_u = y + 0.5*dy
    x_eta = x + 0.5*dx
    y_eta = y + 0.5*dy
    
    X_u, Y_u = np.meshgrid(x_u, y_u)
    X_v, Y_v = np.meshgrid(x_v, y_v)
    X_ets, Y_ets = np.meshgrid(x_eta, y_eta)

    eta, u, v = f3.init_arakawa_C(nx, ny)

    t = dt*nt
    energy = [] 

    for it in range(nt): 
        
       # The boundary condition
       u[:,0] = 0
       u[:,-1] = 0
       v[0,:] = 0
       v[-1,:] = 0
        
        
       eta_u = interpolate2D(u, x_u, y_u, X_ets, Y_ets)
       eta_v = interpolate2D(v, x_v, y_v, X_ets, Y_ets)
       etaxdept, etaydept = deptpoint(X_ets, Y_ets, eta_u, eta_v, dt)
       etadept = interpolate2D(eta, x_eta, y_eta, etaxdept, etaydept)
   
       # dudx and dvdy        
       dudx = np.diff(u, axis=1)/dx
       dudx_dept = interpolate2D(dudx, x_eta, y_eta, etaxdept, etaydept)
       dvdy = np.diff(v, axis=0)/dy
       dvdy_dept = interpolate2D(dvdy, x_eta, y_eta, etaxdept, etaydept)

       # Source term on grid point and departure point
       H_div_grid = -H * (dudx + dvdy)*dt
       H_div_dept = -H * (dudx_dept + dvdy_dept)*dt

        
       #  Lagrangian eta
       eta_new = etadept + 0.5 * (H_div_grid + H_div_dept)
      
       detadx = np.diff(eta_new, axis=1)/dx
       detadx = np.pad(detadx, [(0,0),(1,1)], mode='constant')
       detady = np.diff(eta_new, axis=0)/dy
       detady = np.pad(detady, [(1,1),(0,0)], mode='constant')
       
       if it%2 == 1:
          #print(it)
          # u grid

          u_v = interpolate2D(v, x_v, y_v, X_u, Y_u)

          X_u_dept, Y_u_dept = deptpoint(X_u, Y_u, u, u_v, dt)
          u_dept = interpolate2D(u, x_u, y_u, X_u_dept, Y_u_dept)
          v_dept = interpolate2D(v, x_v, y_v, X_u_dept, Y_u_dept)
          tau_x = tau_0*(-np.cos(np.pi*Y_u/L))
          tau_x_dept = tau_0*(-np.cos(np.pi*Y_u_dept/L))
          u_source_grid = ((f_0 + beta*Y_u)*u_v - g*detadx \
                           - gamma*u + tau_x/(rho*H)) * dt

          detadx_dept = interpolate2D(detadx, x_u, y_u, X_u_dept, Y_u_dept)
          u_source_dept = ((f_0 + beta*Y_u_dept)*v_dept \
                           - g*detadx_dept - gamma*u_dept + tau_x_dept/(rho*H)) * dt

          # Lagrangian u
          u = u_dept + 0.5 * (u_source_grid + u_source_dept)
  
          # v grid

          v_u = interpolate2D(u, x_u, y_u, X_v, Y_v)

          X_v_dept, Y_v_dept = deptpoint(X_v, Y_v, v_u, v, dt)
          v_source_grid = (-(f_0 + beta*Y_v)*v_u - g*detady - gamma*v ) * dt 
          u_dept = interpolate2D(u, x_u, y_u, X_v_dept, Y_v_dept)
          detady_dept = interpolate2D(detady, x_v, y_v, X_v_dept, Y_v_dept)
          v_dept = interpolate2D(v, x_v, y_v, X_v_dept, Y_v_dept)  
          v_source_dept = (-(f_0 + beta*Y_v_dept)*u_dept \
                           - g*detady_dept - gamma*v_dept) * dt  
 
          # Lagrangian v
          v = v_dept + 0.5 * (v_source_grid + v_source_dept)
          
       else :
          
          # v grid
          # Calculation of detady
          v_u = interpolate2D(u, x_u, y_u, X_v, Y_v)

          X_v_dept, Y_v_dept = deptpoint(X_v, Y_v, v_u, v, dt)
          v_source_grid = (-(f_0 + beta*Y_v)*v_u - g*detady - gamma*v)*dt 
          u_dept = interpolate2D(u, x_u, y_u, X_v_dept, Y_v_dept)
          detady_dept = interpolate2D(detady, x_v, y_v, X_v_dept, Y_v_dept)
          v_dept = interpolate2D(v, x_v, y_v, X_v_dept, Y_v_dept)  
          v_source_dept = (-(f_0 + beta*Y_v_dept)*u_dept \
                           - g*detady_dept - gamma*v_dept)*dt 

          # Lagrangian v
          v = v_dept + 0.5 *  (v_source_grid + v_source_dept)

          # u grid
          u_v = interpolate2D(v, x_v, y_v, X_u, Y_u)

          X_u_dept, Y_u_dept = deptpoint(X_u, Y_u, u, u_v, dt)
          u_dept = interpolate2D(u, x_u, y_u, X_u_dept, Y_u_dept)
          v_dept = interpolate2D(v, x_v, y_v, X_u_dept, Y_u_dept)

          tau_x = tau_0*(-np.cos(np.pi*Y_u/L))
          tau_x_dept = tau_0*(-np.cos(np.pi*Y_u_dept/L))

          u_source_grid = ((f_0 + beta*Y_u)*u_v - g*detadx \
                           - gamma*u + tau_x/(rho*H))* dt

          detadx_dept = interpolate2D(detadx, x_u, y_u, X_u_dept, Y_u_dept)
          u_source_dept = ((f_0 + beta*Y_u_dept)*v_dept \
                           - g*detadx_dept - gamma*u_dept + tau_x_dept/(rho*H))* dt

          # Lagrangian u
          u = u_dept + 0.5  * (u_source_grid + u_source_dept)
          
       eta = eta_new
       energy.append(f3.Perturbation_energy(u, v, eta, dx, dy))
        

    return u, v, eta, energy


def interpolate2D(para, xarray, yarray, xdepmesh, ydepmesh):
    para_int = interpolate.RegularGridInterpolator\
        ((yarray, xarray), values=para, method = 'linear', \
         bounds_error = False, fill_value = None)

    intpara_array = para_int((ydepmesh, xdepmesh))

    return intpara_array



def deptpoint(X_ets, Y_ets, eta_u_arr, eta_v_arr, dt):
    x_dept = X_ets - eta_u_arr * dt
    y_dept = Y_ets - eta_v_arr * dt
    
    return x_dept, y_dept


def plot_TaskF1(plt_id, u, v, eta, dx , dy, dt, nt, \
               y_lim = [-0.1,0.1], legend_loc= 'upper right'):
    
    x, y, x_u, y_v = f3.Arakawa_C_grid(dx, dy, consts, 'plot')
    x_v = x + 0.5*dx
    y_u = y + 0.5*dy
    x_eta = x + 0.5*dx
    y_eta = y + 0.5*dy
    
    X_u, Y_u = np.meshgrid(x_u, y_u)
    X_v, Y_v = np.meshgrid(x_v, y_v)
    X_eta, Y_eta = np.meshgrid(x_eta, y_eta)
    
    fig2 = plt.figure(plt_id)
    plt2 = plt.contourf(X_u, Y_u, u, cmap=cm.coolwarm)
    cbar1 = fig2.colorbar(plt2, label='u (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('u -Numerical solution  (Semi-Lag 1 Day)')

    fig3 = plt.figure(plt_id+1)
    plt3 = plt.contourf(X_v, Y_v, v, cmap=cm.coolwarm)
    cbar2 = fig3.colorbar(plt3, label='v (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('v -Numerical solution (Semi-Lag 1 Day)')

    fig4 = plt.figure(plt_id+2)
    plt4 = plt.contourf(X_eta, Y_eta, eta, cmap=cm.coolwarm)
    cbar4 = fig4.colorbar(plt4, label='Height (m)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('$\eta$ -Numerical solution (Semi-Lag 1 Day)')

    fig5 = plt.figure(plt_id+3)
    plt4 = plt.plot(x_u, u[0,:],'b',label='u along south boundary')
    plt4 = plt.plot(y_v, v[:,0],'r',label='v along west boundary')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel("wind (ms$^{-1}$)")
    plt.title('Numerical solution (Semi-Lag 1 Day)')
    plt.legend(loc=legend_loc,fontsize= 12)
    
    fig5 = plt.figure(plt_id+4)
    ny_half = int(consts['L']/(2*dy))
    plt5 = plt.plot(x, eta[ny_half,:],'g',label='$\eta$ through the middle of gyre')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel('Height (m)')
    plt.title('Numerical solution (Semi-Lag 1 Day)')
    plt.legend(loc=legend_loc,fontsize= 12)

def plot_TaskF30(plt_id, u, v, eta, dx , dy, dt, nt, \
               y_lim = [-0.1,0.1], legend_loc= 'upper right'):
    
    x, y, x_u, y_v = f3.Arakawa_C_grid(dx, dy, consts, 'plot')
    x_v = x + 0.5*dx
    y_u = y + 0.5*dy
    x_eta = x + 0.5*dx
    y_eta = y + 0.5*dy
    
    X_u, Y_u = np.meshgrid(x_u, y_u)
    X_v, Y_v = np.meshgrid(x_v, y_v)
    X_eta, Y_eta = np.meshgrid(x_eta, y_eta)
    
    fig2 = plt.figure(plt_id)
    plt2 = plt.contourf(X_u, Y_u, u, cmap=cm.coolwarm)
    cbar1 = fig2.colorbar(plt2, label='u (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('u -Numerical solution (Semi-Lag 30 Days)')

    fig3 = plt.figure(plt_id+1)
    plt3 = plt.contourf(X_v, Y_v, v, cmap=cm.coolwarm)
    cbar2 = fig3.colorbar(plt3, label='v (ms$^{-1}$)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('v -Numerical solution (Semi-Lag 30 Days)')

    fig4 = plt.figure(plt_id+2)
    plt4 = plt.contourf(X_eta, Y_eta, eta, cmap=cm.coolwarm)
    cbar4 = fig4.colorbar(plt4, label='Height (m)')
    plt.xlabel("$x$ (m)")
    plt.ylabel("$y$ (m)")
    plt.title('$\eta$ -Numerical solution (Semi-Lag 30 Days)')

    fig5 = plt.figure(plt_id+3)
    plt4 = plt.plot(x_u, u[0,:],'b',label='u along south boundary')
    plt4 = plt.plot(y_v, v[:,0],'r',label='v along west boundary')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel("wind (ms$^{-1}$)")
    plt.title('Numerical solution (Semi-Lag 30 Days)')
    plt.legend(loc=legend_loc,fontsize= 12)
    
    fig5 = plt.figure(plt_id+4)
    ny_half = int(consts['L']/(2*dy))
    plt5 = plt.plot(x, eta[ny_half,:],'g',label='$\eta$ through the middle of gyre')
    plt.ylim(y_lim[0], y_lim[1])
    plt.xlabel("distance (m)")
    plt.ylabel('Height (m)')
    plt.title('Numerical solution (Semi-Lag 30 Days)')
    plt.legend(loc=legend_loc,fontsize= 12)