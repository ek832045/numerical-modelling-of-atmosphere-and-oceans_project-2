import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import uniform_filter
import function as f
import function3 as f3
import Semi_Lag as SL
from __init__ import consts

dx = 10**4   #m
dy = 10**4   #m
dt = 70      #s
nt = int(86400/dt)

eta_0 = -0.11258543674198031 # Found from task below
_, _, u_st, v_st, eta_st = f.steady_state_sol(dx, dy, eta_0)

#Task F Semi Lagrangian
u, v, eta, energy = SL.semi_lagrangian(dx, dy, dt, nt)
SL.plot_TaskF1(0, u, v, eta, dx , dy, dt, nt, \
           y_lim = [-0.02,0.03], legend_loc = 'upper right')

dx = 10**4   #m
dy = 10**4   #m
dt = 70      #s
nt = int(30*86400/dt)

eta_0 = -0.11258543674198031 # Found from task below
_, _, u_st, v_st, eta_st = f.steady_state_sol(dx, dy, eta_0)

#Task F Semi Lagrangian
u, v, eta, energy = SL.semi_lagrangian(dx, dy, dt, nt)
SL.plot_TaskF30(5, u, v, eta, dx , dy, dt, nt, \
           y_lim = [-0.2,0.5], legend_loc = 'upper right')

f3.plot_energy(20, energy, dt, nt, dx)
